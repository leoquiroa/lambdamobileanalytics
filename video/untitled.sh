#!/bin/bash  
set -e  
while read line  
do  
  aws s3 cp $line .
done < filename.txt